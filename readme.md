# Drush Seed
Seed is a heavy-weight script for the mangement of Ubuntu based Drupal development servers. It gives users Drush-based access and management to server-level functionality such as:

* User accounts and SSH keys
* MySQL users and databases
* Drupal installs (known in Seed as _projects_) and aliases
* Apache virtual hosts

## Drush commands provided by Seed

### Server functionality:

    seed-add
Add a virtual host configuration.

    seed-remove
Remove a virtual host configuration.

    seed-list
Display all configured virtual hosts.

    seed-status
Display the current status (running/not running) of the server.

    seed-start
Start the web server.

    seed-stop
Stop the web server.

    seed-restart
Restart the web server.

    seed-check-config
Check the syntax of the generated configuration files.

    seed-error-log
Display the error log using `tail`.

    seed-install
Install the "seed" symlink.

### Project functionality:

    seed-init
Initializes a new project.

    seed-bare
Initializes a bare repository with structure.

    seed-delete
Deletes a project.

    seed-symlink-files
Symlinks the project's files directory to the "global" project.

    seed-sync
Synchronize code, files and databases between aliases.

    seed-copy
Copy a project from one user to another.

    seed-sync
Synchronize (code, database and/or files) between two aliases.

### User functionality:

    seed-user-add
Adds a system user.

    seed-user-delete
Adds a system user.

    seed-user-add-key
Adds a SSH key for a user.

### Database functionality:

    seed-db-user-add
Adds a database user.

    seed-db-user-delete
Deletes a database user and optionally their databases.

    seed-db-add
Adds a database.

    seed-db-delete
Deletes databases.

    seed-db-dump
Dumps a database.

    seed-db-load
Loads a databases.

### Alias functionality:

    seed-add-alias
Add a Drush alias.

### Additional functionality:

    seed-test
Run the tests.

