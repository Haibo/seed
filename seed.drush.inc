<?php

/**
 * Seed is a light-weight, minimal configuration script for running an Apache
 * http server in a development environment.
 */

// Load components (command callbacks) and plugins (defined in the .info file)
seed_include('util.inc', 'components');
seed_include('server.inc', 'components');
seed_include('project.inc', 'components');
seed_include('user.inc', 'components');
seed_include('db.inc', 'components');
seed_include('alias.inc', 'components');
seed_load_plugins();

/**
 * Implementation of hook_drush_help().
 */
function seed_drush_help($section) {
  switch ($section) {
    case 'meta:seed:title':
      return dt("Seed HTTP server commands");
    case 'meta:seed:summary':
      return dt('Start and stop the server, add and remove virtual hosts.');
  }
}

/**
 * Implementation of hook_drush_command().
 */
function seed_drush_command() {
  $items['seed'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'aliases' => array('server'),
  );

  $items['seed-start'] = array(
    'description' => 'Start the web server.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'aliases' => array('server-start'),
  );

  $items['seed-stop'] = array(
    'description' => 'Stop the web server.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'aliases' => array('server-stop'),
  );

  $items['seed-restart'] = array(
    'description' => 'Restart the web server.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'aliases' => array('server-restart'),
  );

  $items['seed-status'] = array(
    'description' => 'Display the current status (running/not running) of the server.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'aliases' => array('server-status'),
  );

  $items['seed-check-config'] = array(
    'description' => 'Check the syntax of the generated configuration files.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'aliases' => array('server-check-config'),
  );

  $items['seed-error-log'] = array(
    'description' => 'Display the error log using `tail`.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'aliases' => array('server-error-log'),
  );

  $items['seed-list'] = array(
    'description' => 'Display all configured virtual hosts.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'aliases' => array('server-list'),
  );

  $items['seed-add'] = array(
    'description' => 'Add a virtual host configuration.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'aliases' => array('server-add'),
  );

  $items['seed-remove'] = array(
    'description' => 'Remove a virtual host configuration.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'aliases' => array('seed-rm', 'server-remove', 'server-rm'),
  );

  $items['seed-install'] = array(
    'description' => 'Install the "seed" symlink.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-init'] = array(
    'description' => 'Initializes a new project.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-bare'] = array(
    'description' => 'Initializes a bare repository with structure.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-delete'] = array(
    'description' => 'Deletes a project.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-copy'] = array(
    'description' => 'Copy a project from one user to another.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-symlink-files'] = array(
    'description' => 'Deletes a project.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-user-add'] = array(
    'description' => 'Adds a system user.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'aliases' => array('add-user'),
  );

  $items['seed-user-delete'] = array(
    'description' => 'Adds a system user.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'aliases' => array('delete-user'),
  );

  $items['seed-user-add-key'] = array(
    'description' => 'Adds a SSH key for a user.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'aliases' => array('add-user-key'),
  );

  $items['seed-db-user-add'] = array(
    'description' => 'Adds a database user.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'aliases' => array('seed-db-add-user'),
  );

  $items['seed-db-user-delete'] = array(
    'description' => 'Deletes a database user and optionally their databases.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'aliases' => array('seed-db-delete-user'),
  );

  $items['seed-db-add'] = array(
    'description' => 'Adds a database.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-db-delete'] = array(
    'description' => 'Deletes databases.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-db-dump'] = array(
    'description' => 'Dumps project database.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-db-load'] = array(
    'description' => 'Loads a project database.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-add-alias'] = array(
    'description' => 'Add a Drush alias.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-test'] = array(
    'description' => 'Callback for testing functionality.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-sync'] = array(
    'description' => 'Synchronize projects between hosts.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  return $items;
}

/**
 * Include a file from the seed includes directory.
 */
function seed_include($filename, $directory = 'includes') {
  $file = dirname(__FILE__) .'/' . $directory . '/'. $filename;
  if (file_exists($file)) {
    include_once $file;
  }
}

/**
 * Load the config file and include any plugins that are enabled.
 */
function seed_load_plugins() {
  $config = seed_get_config();

  foreach ($config['plugins'] as $plugin) {
    seed_include($plugin . '.inc', 'plugins');
  }
}

/**
 * Command callback for `drush seed`.
 */
function drush_seed() {
  $args = func_get_args();
  $command = array_shift($args);
  if (empty($command)) {
    $command = 'status';
  }

  $commands = drush_get_commands();
  $command = $commands['seed-'. $command];

  drush_dispatch($command, $args);
}

/**
 * Command callback for `drush seed-test`.
 *
 * A basic test suite runner. Tests are under the ./tests directory and are
 * written in classes named Seed<Example>Test. Any methods in those classes
 * prefixed with "test" (i.e. testAnExample()) are run automatically. Results
 * are stored by the static SeedTest class (see ./tests/base.inc) and
 * reported once all tests are finished.
 *
 * These tests should cover basic Seed functionality and where possible
 * should not require input, however there are certain situations, such as
 * where functionality is handled by system functions (i.e. adduser) that
 * user input is required.
 *
 * To run only certain tests, pass them in as arguments.
 */
function drush_seed_test() {
  $args = func_get_args();

  seed_include('base.inc', 'tests');
  if ($handle = opendir(dirname(__FILE__) . '/tests')) {
    while (false !== ($entry = readdir($handle))) {
      if ($entry != "." && $entry != ".." && $entry != 'base.inc') {
        seed_include($entry, 'tests');

        $name = array_shift(explode('.', $entry));

        // We have arguments but this test isn't in them, so continue.
        if (count($args) && !in_array($name, $args)) {
          continue;
        }

        $class = 'Seed' . ucfirst($name) . 'Test';
        $instance = new $class;

        $class_methods = get_class_methods($instance);
        foreach ($class_methods as $method_name) {
          if (substr($method_name, 0, 4) === 'test') {
            drush_log('[Test] Running ' . $class . '::' . $method_name, 'ok');
            $instance->$method_name();
          }
        }
      }
    }
    closedir($handle);
  }
  drush_log('---------------------', 'ok');
  $status = (SeedTest::$fails === 0) ? 'ok' : 'warning';
  drush_log('[Test] ' . SeedTest::$fails . ' Failures', $status);
  drush_log('[Test] ' . SeedTest::$passes . ' Passes', $status);
}

/**
 * An invocation system similar to Drupal's hooks. Command callbacks can call
 * seed_invoke('seed_a_hook', $a). Any plugin implementing the hook (i.e.
 * <PluginName>_seed_a_hook($a)) will be called automatically. For example
 * we are shipping 1.0 with Bitbucket functionality which responds to user SSH
 * keys being created by pushing them to Bitbucket and deleting them when the
 * user is deleted.
 *
 * @todo Does Drush already implement something like this, can we use that?
 * @todo If not, we're going to have to add more seed_invoke() calls
 * throughout the codebase.
 *
 * @param string $hook The hook to invoke.
 */
function seed_invoke($hook) {
  static $funcs;
  if (!isset($funcs)) {
    $all = get_defined_functions();
    $funcs = $all['user'];
  }
  $args = func_get_args();
  unset($args[0]);
  foreach ($funcs as $function) {
    if (stripos($function, $hook) !== FALSE) {
      call_user_func_array($function, $args);
    }
  }
}

/**
 * Loads, parses and returns the config file.
 *
 * @param mixed[] $replacements Map of replacements to make. Allows tokens to
 * be used in configuration values.
 *
 * @return A map of configuration names and values.
 */
function seed_get_config($replacements = array()) {
  static $config;
  if (!isset($config)) {
    if (file_exists(dirname(__FILE__) . '/seed.info')) {
      $temp = parse_ini_file(dirname(__FILE__) . '/seed.info');
      // Static variable replacement, only needs to happen once
      foreach ($temp as $key => &$value) {
        if (is_string($value)) {
          preg_match_all('/(\[THIS.([a-zA-Z0-9\/\.-_]+?)\])/', $value, $matches);
          foreach ($matches[0] as $idx => $target) {
            $value = str_replace($target, $temp[$matches[2][$idx]], $value);
          }
        }
      }
      $config = $temp;
    }
    else {
      drush_log(dt('Could not load the global config file.'), 'error');
      exit;
    }
  }

  // Dynamic variable replacement, could happen multiple times as a result of
  // functions calling other functions.
  if (count($replacements)) {
    $arr1 = $arr2 = array();
    foreach ($replacements as $pattern => $replacement) {
      $arr1[] = $pattern;
      $arr2[] = $replacement;
    }
    $temp = $config;
    foreach ($temp as $key => $value) {
      $temp[$key] = preg_replace($arr1, $arr2, $value);
    }
    return $temp;
  }
  else {
    return $config;
  }
}

/**
 * Get a static instance of the server object.
 *
 * @return Static SeedServer instance.
 */
function seed_get_server() {
  static $server;
  if (!isset($server)) {
    seed_include('server.inc');
    $server = new SeedServer();
  }
  return $server;
}

/**
 * Get an instance of a HTTP configuration file object.
 *
 * @return SeedHttpdConfFile instance.
 */
function seed_get_conf_file() {
  seed_include('conf-file.inc');
  $conf_file = new SeedHttpdConfFile();
  return $conf_file;
}

/**
 * Command callback for `drush seed-install`.
 */
function drush_seed_install() {
  $message = <<<DT
This command will attempt to install a symbolic link that will allow you to use
`seed` instead of `drush seed`.  In order for this to work, the specified
install path must be in your shell's PATH.
Install path
DT;

  $path = drush_prompt($message, '/usr/local/bin');
  if (is_dir($path) && is_writable($path)) {
    $source = dirname(__FILE__) .'/seed.drush';
    $destination = $path .'/seed';
    if (symlink($source, $destination)) {
      drush_log("[Installed] $destination", 'ok');
    }
  }
}
