<?php

seed_include('base.inc');

/**
 * Handles the management of projects.
 */
class SeedProject extends SeedBase {
  protected $attributes = array(
    'name' => NULL,
    'user' => NULL,
    'config' => NULL,
    'dir' => NULL,
  );

  /**
   * Stores the name of the project and the user it belongs to, as well as
   * an instance of the configuration with the project and user tokens
   * replaced.
   *
   * @param string $name Name of the project.
   * @param string $user Name of the user the project belongs to.
   */
  public function __construct($name, $user) {
    $this->name = $name;
    $this->user = $user;

    $this->config = seed_get_config(array(
      '/\[PROJECT\]/' => $name,
      '/\[USER\]/' => $user
    ));

    // We access this a lot, so create a handy shortcut.
    $this->dir = $this->config['project_directory'];
  }

  /**
   * Check to see if the project directory exists.
   *
   * @return bool Whether or not the directory exists.
   */
  public function dir_exists() {
    return seed_util_dir_exists($this->dir);
  }

  /**
   * Create the project directory.
   */
  public function create_dir() {
    if (!$this->dir_exists()) {
      seed_util_dir_create($this->dir);
      drush_log(dt('[Project] Successfully created directory !dir', array('!dir' => $this->dir)), 'ok');
    }
    else {
      return drush_set_error('SEED_PROJECT_DIR_ERROR', dt('[Project] Unable to create directory !dir, already exists.', array('!dir' => $this->dir)));
    }
  }

  /**
   * Delete the project directory.
   */
  public function delete_dir() {
    if (!$this->dir_exists()) {
      return drush_set_error('SEED_PROJECT_DIR_ERROR', dt('[Project] Unable to delete directory !dir, does not exist.', array('!dir' => $this->dir)));
    }
    else {
      drush_shell_exec('rm -rf ' . $this->dir);
      drush_log(dt('[Project] Successfully deleted directory !dir', array('!dir' => $this->dir)), 'ok');
    }
  }

  /**
   * Clone a repository in to the project directory.
   *
   * @param string $repo URI of the repository to clone.
   */
  public function clone_repository($repo) {
    drush_log(dt('[Project] Cloning repository !repo', array('!repo' => $repo)), 'ok');
    drush_log(dt('[Project] This could take awhile depending on the size of the repository...'), 'ok');

    $status = drush_shell_exec_interactive('git clone ' . $repo . ' ' . $this->dir);
    if (!$status) {
      return drush_set_error('SEED_PROJECT_CLONE_ERROR', dt('[Project] Cloning respository !repo failed.', array('!repo' => $repo)));
    }

    drush_log(dt('[Project] Cloning repository complete'), 'ok');
  }

  /**
   * Create a bare git repository in the project directory.
   *
   * @param bool $gitignore Create a .gitignore file.
   * @param bool $remote Set a remote for the repository.
   */
  public function bare_repository($gitignore, $remote) {
    $status = drush_shell_cd_and_exec($this->dir, 'git init .');
    if (!$status) {
      return drush_set_error('SEED_PROJECT_GIT_ERROR', dt('[Project] Unable to initialize a Git repository in !dir.', array('!dir' => $this->dir)));
    }
    drush_log(dt('[Project] Initialized git repository'), 'ok');

    if ($gitignore) {
      file_put_contents($this->dir . '/.gitignore', $this->config['repo_gitignore']);
      drush_log(dt('[Project] Created .gitignore file'), 'ok');
    }

    if ($remote) {
      $url = $this->config['project_repo_template'];

      $status = drush_shell_exec($this->config['repo_init_cmd']);
      if (!$status) {
        drush_log(dt('[Project] Could not initialize new remote:'), 'error');
        seed_util_error_print(drush_shell_exec_output());
      }
      drush_log(dt('[Project] Initialized git remote'), 'ok');

      $status = drush_shell_cd_and_exec($this->dir, 'git remote add origin ' . $url);
      if (!$status) {
        drush_log(dt('[Project] Could not add remote:'), 'error');
        seed_util_error_print(drush_shell_exec_output());
      }
      drush_log(dt('[Project] Added remote !remote', array('!remote' => $url)), 'ok');
    }
  }

  /**
   * Check to see if the current branch for two projects are the same.
   *
   * @param SeedProject $compare The project to compare with.
   *
   * @return bool Whether or not the branches are the same.
   */
  public function compare_branches(SeedProject $compare) {
    drush_shell_cd_and_exec($this->dir, 'echo $(git branch | grep "*" | sed "s/* //")');
    $this_branch = reset(drush_shell_exec_output());

    drush_shell_cd_and_exec($compare->dir, 'echo $(git branch | grep "*" | sed "s/* //")');
    $compare_branch = reset(drush_shell_exec_output());

    return $this_branch == $compare_branch;
  }

  /**
   * Create the directory structure in a project.
   *
   * @param bool $create_db_dir Create the 'db' directory.
   */
  public function create($create_db_dir) {
    $dirs_to_create = array('logs', 'public');
    if ($create_db_dir) $dirs_to_create[] = 'db';

    foreach ($dirs_to_create as $dir) {
      $status = seed_util_dir_create($this->dir . '/' . $dir);
      if (!$status) {
        return drush_set_error('SEED_PROJECT_DIR_ERROR', dt('[Project] Could not create !dir directory.', array('!dir' => $dir)));
      }
      drush_log(dt('[Project] Created !dir directory', array('!dir' => $dir)), 'ok');
    }
  }

  /**
   * Copy the file structure from another project.
   *
   * @param SeedProject $original The project to copy files from.
   */
  public function copy(SeedProject $original) {
    $status = drush_shell_exec('cp -rfa ' . $original->dir . ' ' . $this->dir);
    if (!$status) {
      drush_log(dt('[Project] Could not copy files to the directory !dir:', array('!dir' => $this->dir)), 'error');
      seed_util_error_print(drush_shell_exec_output());
    }

    drush_shell_exec('sudo chmod -R g+w ' . $this->dir);
    drush_shell_exec('sudo chown -R ' . $this->user . ' ' . $this->dir);

    drush_log(dt('[Project] Copied !dir', array('!dir' => $this->dir)), 'ok');
  }

  /**
   * Symlink the files directory of a project to the files of another project.
   *
   * @param SeedProject $target The project with the files directory that
   * will be symlinked to.
   */
  public function symlink_files(SeedProject $target) {
    $target_dir = $target->dir . '/public/sites/default/files';
    $local_dir = $this->dir . '/public/sites/default/files';
    if (seed_util_dir_exists($local_dir)) {
      return drush_set_error('SEED_PROJECT_FILES_EXIST', dt('[Project] Files directory exists already, aborting symlink.'));
    }

    if (!seed_util_dir_exists($target_dir)) {
      if (!seed_util_dir_create($target_dir)) {
        return drush_set_error('SEED_PROJECT_NO_TARGET_FILES_DIR', dt('[Project] Could not create the directory !target.', array('!target' => $target_dir)));
      }
    }

    $status = drush_shell_exec('ln -s ' . $target_dir . ' ' . $local_dir);
    if (!$status) {
      return drush_set_error('SEED_PROJECT_SYMLINK_ERROR', dt('[Project] Cannot symlink !dir to !target.', array('!dir' => $local_dir, '!target' => $target_dir)));
    }
    drush_log(dt('[Project] Symlinked !dir to !target', array('!dir' => $local_dir, '!target' => $target_dir)), 'ok');
  }

  /**
   * Delete a project directory.
   */
  public function delete() {
    drush_shell_exec('rm -rf ' . $this->dir);
    drush_log(dt('[Project] !dir has been deleted', array('!dir' => $this->dir)), 'ok');
  }
}
