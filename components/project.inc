<?php

seed_include('project.inc');

/**
 * Command callback for `drush seed-init`.
 */
function drush_seed_init($project_name = '') {
  $config = seed_get_config();

  if (empty($project_name)) {
    $project_name = drush_prompt(dt('Project name'));
  }

  $project = new SeedProject($project_name, $_SERVER['USER']);

  // Don't let the user override an existing dir.
  if ($project->dir_exists()) {
    return drush_set_error('SEED_PROJECT_EXISTS', dt('The directory !dir already exists, exiting to prevent any overriding.', array('!dir' => $project->dir)));
  }

  // Create the project directory.
  $project->create_dir();

  // Present the user with a choice of how to proceed.
  $clone_or_init = drush_choice(array(
    'Clone an existing project repository',
    'Create a new project'
  ));

  // Clone a repository.
  if ($clone_or_init === 0) {
    drush_seed_clone($project);
    seed_invoke('seed_clone_complete', $project->dir);
  }
  // Create a new repository.
  else if ($clone_or_init === 1) {
    drush_seed_bare($project);
  }
  else {
    // Cleanup the user directory we already created.
    $project->delete_dir();
    return drush_set_error('SEED_PROJECT_BAILED', dt('Creation of project cancelled.'));
  }

  $global_project = new SeedProject($project_name, $config['global_user']);

  // Prompt to see if we should be transfering this to the global user.
  if (!$global_project->dir_exists() && drush_confirm(dt('Add site to !global?', array('!global' => $project->config['project_global_directory'])))) {
    $global_project->copy($project);
  }

  // If the global directory already exists...
  if ($clone_or_init === 0 && $global_project->dir_exists()) {
    // Compare the Git branches.
    $branches_match = $global_project->compare_branches($project);
    if (!$branches_match) {
      drush_log(dt('[Project] The git branch in !project differs from !global', array('!project' => $project->dir, '!global' => $global_project->dir)), 'warning');
    }

    // If they match, offer to symlink files.
    if ($branches_match && drush_confirm(dt('Symlink the files directory for this project to the global project?'))) {
      $project->symlink_files($global_project);
    }
  }

  // Add the global database user.
  // Chances are this has been run before but it will fail silently when it
  // sees $config['repo_global_db_user']. Not too concerned about the overhead.
  drush_seed_db_user_add($config['project_global_db_user']);

  // Add a database for this project for the global user.
  // This is non-optional.
  drush_seed_db_add($project_name, $config['project_global_db_user']);

  // Check to see if we want to create a project database for this user.
  if (drush_confirm('Create database ' . $project_name . '_' . $_SERVER['USER'] . '?')) {
    drush_seed_db_add($project_name, $_SERVER['USER']);
  }

  if (drush_confirm('Create Drush alias?')) {
    seed_add_alias_dev($project_name, $_SERVER['USER']);
  }

  // Handle adding the vhost.
  drush_seed_add($project_name);
}

/**
 * Command callback for `drush seed-clone`.
 */
function drush_seed_clone($project = NULL) {
  if (is_string($project)) {
    $project = new SeedProject($project, $_SERVER['USER']);
  }
  else if (!is_object($project) || (is_object($project) && get_class($project) !== 'SeedProject')) {
    $project = drush_prompt(dt('Project name'));
    $project = new SeedProject($project, $_SERVER['USER']);
  }

  $repo = drush_prompt(dt('Repository URL'), $project->config['project_repo_template']);

  $project->clone_repository($repo);
}

/**
 * Command callback for `drush seed-bare`.
 *
 * Initialize the basic project directory structure, giving the user to set up
 * git now and add a remote based on their config.
 */
function drush_seed_bare($project = NULL) {
  if (is_string($project)) {
    $project = new SeedProject($project, $_SERVER['USER']);
  }
  else if (!is_object($project) || (is_object($project) && get_class($project) !== 'SeedProject')) {
    $project = drush_prompt(dt('Project name'));
    $project = new SeedProject($project, $_SERVER['USER']);
  }

  $create_db_dir = drush_confirm(dt('Create "db" directory?'));

  $project->create($create_db_dir);

  if (drush_confirm(dt('Use git?'))) {
    $gitignore = drush_confirm(dt('Create ".gitignore" file?'));
    $remote = drush_confirm(dt('Initialize new remote: !remote ?', array('!remote' => $project->config['project_repo_template'])));

    $project->bare_repository($gitignore, $remote);
  }
}

/**
 * Command callback for `drush seed-delete`.
 */
function drush_seed_delete($project = NULL) {
  $config = seed_get_config();

  if (is_string($project)) {
    $project = new SeedProject($project, $_SERVER['USER']);
  }
  else if (!is_object($project) || (is_object($project) && get_class($project) !== 'SeedProject')) {
    $project = drush_prompt(dt('Project name'));
    $project = new SeedProject($project, $_SERVER['USER']);
  }

  if (drush_confirm(dt('Are you sure you wish to delete !dir?', array('!dir' => $project->dir)))) {
    $project->delete();
  }

  $global_project = new SeedProject($project->name, $config['global_user']);

  if($global_project->dir_exists()) {
    if (drush_confirm(dt('Delete !dir?', array('!dir' => $global_project->dir)))) {
      $global_project->delete();
    }
  }

  drush_seed_remove($project->name);
}

/**
 * Command callback for `drush seed-symlink-files`.
 */
function drush_seed_symlink_files($project_name = NULL, $project_user = NULL, $target_project_name = NULL, $target_user = NULL) {
  $config = seed_get_config();

  if (empty($project_name)) {
    $project_name = drush_prompt(dt('Project name'));
  }

  if (empty($project_user)) {
    $project_user = drush_prompt(dt('Project user'), $_SERVER['user']);
  }

  if (empty($target_project_name)) {
    $target_project_name = drush_prompt(dt('Target project'), $project_name);
  }

  if (empty($target_user)) {
    $target_user = drush_prompt(dt('Target user'), $config['global_user']);
  }

  $project = new SeedProject($project_name, $project_user);
  $target_project = new SeedProject($target_project_name, $target_user);

  if (drush_confirm(dt('Symlink the files directory for !project (user !project_user) to the files directory of !target_project (user !target_user)?', array('!project' => $project_name, '!project_user' => $project_user, '!target_project' => $target_project_name, '!target_user' => $target_user)))) {
    $project->symlink_files($target_project);
  }
}

/**
 * Command callback for `drush seed-copy`.
 */
function drush_seed_copy($from_project_name = NULL, $from_project_user = NULL, $to_project_name = NULL, $to_project_user = NULL) {
  if (empty($from_project_name)) {
    $from_project_name = drush_prompt(dt('"From" project name'));
  }

  if (empty($from_project_user)) {
    $from_project_user = drush_prompt(dt('"From" user'), $_SERVER['user']);
  }

  if (empty($to_project_name)) {
    $to_project_name = drush_prompt(dt('"To" project name'), $from_project_name);
  }

  if (empty($to_project_user)) {
    $to_project_user = drush_prompt(dt('"To" user'));
  }

  $from_project = new SeedProject($from_project_name, $from_project_user);

  $to_project = new SeedProject($to_project_name, $to_project_user);
  $to_project->copy($from_project);
}

/**
 * Command callback for `drush seed-sync`.
 */
function drush_seed_sync($project = NULL) {
  if (empty($project)) {
    $project = drush_prompt(dt('Project name'));
  }

  $alias = drush_sitealias_get_record('@' . $project);
  $sites = $alias['site-list'];

  if (!count($sites)) {
    return drush_set_error('SEED_PROJECT_SYNC_NO_ALIAS', dt('No aliases have been defined for !project.', array('!project' => $project)));
  }

  sort($sites);

  $selection = drush_choice($sites, dt('Where should we sync from?'));
  $from_alias = $sites[$selection];

  if ($selection === FALSE) {
    return drush_set_error('SEED_PROJECT_SYNC_NO_SELECTION', dt('No selection made.'));
  }

  // Destination can't be the same as source.
  unset($sites[$selection]);

  $selection = drush_choice($sites, dt('Where should we sync to?'));
  $to_alias = $sites[$selection];

  if ($selection === FALSE) {
    return drush_set_error('SEED_PROJECT_SYNC_NO_SELECTION', dt('No selection made.'));
  }

  $selection = drush_choice_multiple(array('code' => 'Code', 'files' => 'Files', 'db' => 'Database'), FALSE, dt('What should be synced?'));

  if ($selection === FALSE) {
    return drush_set_error('SEED_PROJECT_SYNC_NO_SELECTION', dt('No selection made.'));
  }

  seed_invoke('seed_pre_sync', $from_alias, $to_alias, $selection);
  // A plugin has raised an error, halt execution.
  if (drush_get_error()) {
    return;
  }

  drush_shell_exec("drush $to_alias cc all");

  if (array_search('code', $selection) !== FALSE) {
    drush_shell_exec_interactive("drush rsync --exclude-files $from_alias $to_alias");
  }

  if (array_search('files', $selection) !== FALSE) {
    $from_files_dir = drush_prompt(dt("From files directory"), 'sites/default/files');
    $to_files_dir = drush_prompt(dt("To files directory"), 'sites/default/files');
    drush_shell_exec_interactive("drush rsync $from_alias:$from_files_dir $to_alias:$to_files_dir");
  }

  if (array_search('db', $selection) !== FALSE) {
    drush_shell_exec_interactive("drush sql-sync $from_alias $to_alias");
  }

  drush_shell_exec("drush $to_alias cc all");

  seed_invoke('seed_post_sync', $from_alias, $to_alias, $selection);

  drush_log(dt('[Project] Seed sync complete'), 'ok');
}
