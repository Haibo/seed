<?php

/**
 * Check to see if a directory exists.
 *
 * @param string $dir Path to the directory to check.
 * @return bool Whether or not the directory exists.
 */
function seed_util_dir_exists($dir) {
  drush_shell_exec("[ -d $dir ] && echo '1' || echo '0'");

  return (bool) reset(drush_shell_exec_output());
}

/**
 * Thin wrapper to create directories. -p flag to create the parent hierarchy.
 *
 * @param string $dir Directory to create.
 * @param bool $use_sudo Whether or not to use sudo to create the directory.
 * @param string $username chown to this user after creation.
 * @return bool Whether or not the directory was created.
 */
function seed_util_dir_create($dir, $use_sudo = FALSE, $username = '') {
  $config = seed_get_config();

  $prefix = ($use_sudo) ? 'sudo ' : '';
  $status = drush_shell_exec($prefix . 'mkdir -m 0775 -p ' . $dir);
  drush_shell_exec($prefix . 'chgrp ' . $config['global_group'] . ' ' . $dir);

  if (!empty($prefix) && !empty($username)) {
    drush_shell_exec($prefix . 'chown ' . $username . ' ' . $dir);
  }

  if (!$status) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Given the return of drush_shell_exec_output(), print the output.
 *
 * @param array $output Array of strings to be output.
 */
function seed_util_error_print($output) {
  if (!empty($output)) {
    foreach ($output as $line) {
      drush_print($line);
    }
  }
}

/**
 * Find all databases with a name that matches a pattern.
 *
 * @param string $pattern Pattern to select databases. May use % for wildcards.
 * @param SeedDB $db Object to manage database connection.
 * @return array An array of databases matching the pattern.
 */
function seed_util_db_matching_pattern($pattern, SeedDB $db = NULL) {
  if ($db === NULL) {
    $config = seed_get_config();
    $dbh = new PDO('mysql:dbname=mysql;host=' . $config['mysql_host'], $config['mysql_user'], $config['mysql_pass']);
  }
  else {
    $dbh = $db->dbh;
  }

  $list = array();
  foreach($dbh->query("SHOW DATABASES LIKE '$pattern'") as $db) {
    $list[$db[0]] = $db[0];
  }

  return $list;
}

/**
 * Gets all the aliases currently defined. Wraps Drush for clarity.
 *
 * @return array An array of aliases defined.
 */
function seed_util_get_aliases() {
  $aliases = _drush_sitealias_all_list();

  return $aliases;
}