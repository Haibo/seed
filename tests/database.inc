<?php

class SeedDatabaseTest {
  /**
   * Tests the creation and deletion of database users.
   */
  public function testUserCreateAndDelete() {
    $db = new SeedDB();
    $db->user_add('test_db_user');

    SeedTest::assert($db->user_exists('test_db_user', TRUE), TRUE);

    $db->user_delete('test_db_user');

    SeedTest::assert($db->user_exists('test_db_user', TRUE), FALSE);
  }

  /**
   * Tests the creation and deletion of databases.
   */
  public function testDatabaseCreateAndDelete() {
    $db = new SeedDB();
    $db->user_add('test_db_user');
    $db->database_add('test_database_test_db_user', 'test_db_user');

    $dbs = seed_util_db_matching_pattern('test_database_test_db_user', $db);
    SeedTest::assertGreaterThan(count($dbs), 0);

    $db->database_delete('test_database_test_db_user');

    $dbs = seed_util_db_matching_pattern('test_database_test_db_user', $db);
    SeedTest::assertLessThan(count($dbs), 1);

    $db->user_delete('test_db_user');
  }

  /**
   * Tests the database dumping and loading functionality.
   */
  public function testDatabaseDumpAndLoad() {
    $db = new SeedDB();
    $db->user_add('test_db_user');
    $db->database_add('test_database_test_db_user', 'test_db_user');

    $dbh = $db->dbh;
    $dbh->query("CREATE TABLE `test_database_test_db_user`.`test` (id INT, data VARCHAR(100))");

    // Create some test data.
    for ($i = 1; $i <= 100; $i++) {
      $dbh->query("INSERT INTO `test_database_test_db_user`.`test` VALUES ($i, 'Test $i')");
    }

    $db->database_dump('test_database_test_db_user', 'test_db_user');

    SeedTest::assertFileExistsByPattern('/home/test_db_user/www/test/db/*.gz');

    $file = reset(glob('/home/test_db_user/www/test/db/*.gz'));

    $db->database_load($file, 'test_database_test_db_user', 'test_db_user');

    $query = $dbh->query("SELECT COUNT(*) FROM `test_database_test_db_user`.`test`");
    $count = $query->fetchColumn();

    SeedTest::assert((int) $count, 100);

    drush_shell_exec('sudo rm -rf /home/test_db_user');
    $db->database_delete('test_database_test_db_user');
    $db->user_delete('test_db_user');
  }
}